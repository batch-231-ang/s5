class Customer{
	constructor(email){
		if(typeof email == "string" && email.includes("@") && email.includes(".com")){
			this.email = email;
		}else{
			console.log("Provide a valid email");
			this.email = undefined;
		}
		
		this.cart = new Cart();
		this.orders = [];
	}
	checkout(){
		if(this.cart != null){
			this.orders.push({
				product: this.cart.contents,
				totalAmount: this.cart.totalAmount
			});
			this.cart = new Cart();
		}else{
			console.log("Cart is empty");
		}
		return this;
	}
}

class Product{
	constructor(name, price){
		if(typeof name == "string" && typeof price == "number"){
			this.name = name;
			this.price = price;
		}else{
			console.log("Provide valid values for the Product.");
			this.name = undefined;
			this.price = 0;
		}
		
		this.isActive = true;
	}
	archive(){
		this.isActive = false;
		return this;
	}
	updatePrice(updatedPrice){
		if(typeof updatedPrice == "number"){
			this.price = updatedPrice;
		}else{
			console.log("Please input a number");
		}
		return this;
	}
}


class Cart{
	constructor(){
		this.contents = [];
		this.totalAmount = 0;
	}
	addToCart(product, qty){
		if(typeof product == "object" && typeof qty == "number"){
			if(product.isActive === true){
				this.contents.push({
					product: product,
					quantity: qty
				});
			}else{
				console.log("Item is currently unavailable.");
			}
			
		}else{
			console.log("Please input correct values");
		}
		return this;
	}
	showCartContents(){
		if(this.contents.length > 0){
			console.log(this.contents.length);
			console.log(this.contents);
		}else{
			console.log("Cart is empty");
		}
		
		return this;
	}
	updateProductQuantity(productName, newQty){
		if(typeof productName == "string" && typeof newQty == "number"){
			let product = this.contents.find(item => item.product.name.toLowerCase() === productName.toLowerCase());
			if(product !== undefined){
				product.quantity = newQty;	
			}else{
				console.log("Item does not exist");
			}
			
		}else{
			console.log("Please input correct values");
		}
		return this;
	}
	clearCartContents(){
		this.contents = [];
		this.totalAmount = 0;
		return this;
	}
	computeTotal(){
		let currentAmount = 0;
		this.contents.forEach(item => {
			currentAmount += (item.product.price * item.quantity);
		});
		this.totalAmount = currentAmount;
		return this;
	}
}


//----------Testing statements------------------
const customer = new Customer("cutomer@mail.com");

const rpi = new Product("RPi", 1500);
const esp = new Product("ESP", 200);
const arduino = new Product("Arduino", 200);
const breadboard = new Product("Breadboard", 200);

customer.cart.addToCart(rpi, 2);
customer.cart.addToCart(esp, 5);

//customer
/*
	customer.checkout()
*/

//products
/*
	rpi.archive()
	rpi.updatePrice(100)
*/

//cart
	
/*
	customer.cart.computeTotal()
	customer.cart.updateProductQuantity("ESP", 10)
	customer.cart.showCartContents()
	customer.cart.clearCartContents()
	customer.cart.showCartContents()
	customer.checkout()
*/